# Dock2PMCenter  - Telegram Chatbot on Docker

一个帮你处理私人聊天消息的 Telegram 机器人。现以Docker形态再次降临。

## 1.  简介 Easy Introduction

Dock2PMCenter 是 [PMCenter](https://github.com/Elepover/pmcenter) 的Docker版本，诞生目的只为简化Telegram Bot的部署流程，使得Telegram Bot的启动部署更加简洁迅速。

Docker版本的优势；

- 避免安装繁琐的 .Net Core Runtime + .Net Core SDK，真正做到开包即用；
- 通过简单的命令行操作，轻松控制Bot的运行，告别繁琐的启动命令；
- 所有的核心全部包装在Docker镜像中，最大限度避免对系统的破坏

**特别提醒：** 目前的Dock2PMCenter项目仅仅是雏形阶段，不保证稳定性，也不代表最终成品品质！

## 2. 使用方法 Usage

**不建议**直接拉取此镜像！建议使用以下方式启动！

使用以下命令安装 Dock2PMCenter ：

```shell 
curl -fsSL https://ilemonrain.com/download/shell/dock2pmcenter-setup.sh | bash
```

此命令将会自动安装 Dock2PMCenter 相关核心组件，并进行相关的基础配置工作。

在 Dock2PMCenter 安装完成后，使用以下命令控制 Docker2PMCenter ：

> **d2pmc start ：**启动 PMCenter
>
> **d2pmc stop ：**停止 PMCenter
>
> **d2pmc kill ：**强行停止 PMCenter (当stop长时间无响应或失败时使用)
>
> **d2pmc debug ：**使用前台运行方式启动 PMCenter (适合进行调试使用)
>
> **d2pmc setup ：**运行 PMCenter 配置向导
>
> **d2pmc reset：**重置 PMCenter 配置
>
> **d2pmc info： **显示 PMCenter 信息

## 3. 更新日志 Update History

 **2019/04/01 Alpha**

> 雏形&理论测试版本，请不要使用此版本并用于生产环境中！

## 4. 联系方式 Contact

**Telegram： **[@ilemonrain](https://t.me/ilemonrain)

**Telegram Channel： **[@ilemonrain_channel](https://t.me/ilemonrain_channel)

**My Blog： **[https://blog.ilemonrain.com/](https://ilemonrain.com/)

**E-Mail： **ilemonrain#ilemonrain.com